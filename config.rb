require 'zen-grids'
require 'breakpoint'
require 'sass-globbing'

# Location of the theme's resources.
sass_dir        = "src/sass"
css_dir         = "css"
images_dir      = "images"
javascripts_dir = "js"


#add_import_path "wwuzen-businessandsustainability/src/sass/modules"

relative_assets = true
